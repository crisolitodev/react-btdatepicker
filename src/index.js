import React, { Component } from 'react'
import jQuery from 'jquery'
import 'bootstrap-datepicker'
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.min.css'
export class BTDatePicker extends Component {

  constructor(props) {
    super(props)
    this.state = { refresh: false };
  }

  getElement = element => {
    this.$el = jQuery(element).datepicker(this.props.datePickerOptions)
  }

  componentDidMount() {
    this.$el.datepicker(this.props.datePickerOptions)
    if (this.props.onChangeDate) this.$el.on('changeDate', this.props.onChangeDate)

    if (this.props.startDate !== '') this.$el.datepicker( 'setStartDate', this.props.startDate );
    if (this.props.endDate !== '') this.$el.datepicker( 'setEndDate', this.props.endDate);
    this.$el.on('show', this.setState({ refresh: !this.state.refresh }) )
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.startDate !== '') this.$el.datepicker( 'setStartDate', this.props.startDate );
    if (this.props.endDate !== '') this.$el.datepicker( 'setEndDate', this.props.endDate);
  }

  componentWillUnmount() {
    if (this.props.onChangeDate) {
      this.$el.off('changeDate')
    }
    this.$el.off('show')
    this.$el.datepicker('destroy');
  }

  render() {
    var { onChangeDate, datePickerOptions, ...others } = this.props
    return <input {...others} ref={this.getElement} />
  }
}

export default BTDatePicker
