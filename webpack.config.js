var path = require('path');
module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'index.js',
    libraryTarget: 'commonjs2'
  },
  module: {
    rules: [
        {
            test:/\.css$/,
            use:['style-loader','css-loader']
        },
        {
            test: /\.js$/,
            include: path.resolve(__dirname, 'src'),
            exclude: /(node_modules|bower_components|build)/,
            use: {
              loader: 'babel-loader',
              options: {
                presets: [
                  '@babel/preset-env',
                  '@babel/preset-react'
                ],
                plugins:[
                  '@babel/plugin-proposal-class-properties'
                ]
              }
            }
        }
    ]
  },
  externals: [
    'react','react-dom'
  ]
};